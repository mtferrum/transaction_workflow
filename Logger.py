class Logger(object):
    """
        Mock класс для реализации долгосрочного хранения истории транзакций
    """
    @classmethod
    async def log(cls, transaction):
        """
        Некоде долгосрочное хранилище истории всех траназакций
        :param transaction:  объект траназкция
        :return:
        """
        return await transaction.save_state('logged')
