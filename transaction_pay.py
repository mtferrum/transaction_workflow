from uuid import uuid4, UUID
from .wallet import Wallet, Currency
from decimal import Decimal

# К сожалению самая крутая либа для работы с бд в asyncio asyncpg не поддерживает популярные ORM поэтому будем работать
# с SQL напрямую


# маленький ugly hack
tables_columns = {
    'merchant_id': UUID,  # uui транзакции
    'task_id': int,  # id таски обрабатывающей транзакцию
    'state': str,  # состояние транзакции (архиважная штука!)
    'amount': Decimal,  # сумма
    'currency': Currency,  # валюта
    'wallet_key': int,  # id внутреннего счета связанного с транзакцией
}


def type_check(value, tables_columns):
    '''
    Небольшая проверка типов
    Если данный объект не совпадает с данным типом кидается исключение TypeError
    :param value: объект для проверки
    :param tables_columns: колонка в таблице
    :return:
    '''
    if not isinstance(value, tables_columns[tables_columns]):
        raise TypeError(f"table_columns[{tables_columns}] = {type(value)} != {tables_columns[tables_columns]} ")


# описание таблицы
tables_schema = '''
CREATE TABLE transaction_in(
               			merchant_id uuid NOT NULL PRIMARY KEY,         -- uuid транзакции
               			task_id integer,                               -- id таски обрабатывающей 
                        state VARCHAR(10) DEFAULT 'created',           -- состояние транзакции 
                        created timestamp DEFAULT current_timestamp,   -- время создания транзакции
                        amount decimal NOT NULL,                       -- сумма
                        currency VARCHAR(3) NOT NULL,                  -- валюта
                        wallet_key integer NOT NULL,                   -- id внутреннего счета
                      );         
'''

# доступные состояния транзакции

transaction_states = frozenset({'created',  # Транзакция создана - создается БД
                                'pending',  # Создан платеж в платежной системе
                                'canceled',  # Платежная система отклонила платеж
                                'waiting_for_capture',
                                # Платежная система подтверида платеж (ожидается подтвержение сервиса)
                                'succeeded',  # Платежная система провела платеж
                                'fixed',  # Сумма зачислена на внуренний счет
                                'loggeg',  # Траназкция занесена в журнал долгого хранения
                                })


class TransactionAbstractFactory(object):
    '''
        Транзакция - объект маппированный с бд, для коррекного создания нужно для начала убедиться что есть место в бд,
        операции с бд в asyncio идут асинхронно, посему работу с бд в конструктор транзакции поместить не получиться.
        Можно поиграться с метаклассами и __new__ но мы пойдем другим путем через абстрактные фабрики, которые будут
        создавать обеъекты Transaction
    '''

    pool = None  # пул соединений с БД

    def __init__(self, pool):
        self.pool = pool

    async def get(self, merchant_id):
        '''
        Единственный путь получения  транзакции из бд - вызов этого метода
        :param merchant_id: uuid - транзакции
        :return:  сущность транзакции
        '''

        fields = ','.join(tables_columns.keys())

        type_check(merchant_id, 'merchant_id')

        data = f"""
            SELECT ({fields}) 
            FROM transaction_in
            WHERE merchant_id = '{merchant_id}'
        """

        async with self.pool.acquire() as con:
            res = await con.fetchrow(data)
            kwargs = {key: tables_columns[key]([key]) for key in tables_columns.keys()}
            return Transaction(pool=self.pool, **kwargs)

    async def create(self, wallet, amount, currency):
        '''
        Единственный путь создания новой транзакции - вызов этого метов
        :param wallet: Сущность Wallet
        :param amount: Колличество
        :param currency: Валюта
        :return:
        '''

        merchant_id = uuid4()

        if not isinstance(wallet, Wallet):
            raise TypeError

        type_check(amount, 'amount')
        type_check(currency, 'currency')

        data = f"""
            INSERT INTO transaction_in(merchant_id, user_id, amount, currency)
            VALUES('{merchant_id}', {wallet.get_key()}, {amount:.2f}, '{currency}');
              """
        async with self.pool.acquire() as con:
            res = await con.execute(data)
        return self.get(merchant_id=merchant_id)

    async def all(self):
        """
        Возвращает список всех транзакций в базе
        :return:
        """
        data = "SELECT merchant_id FROM transaction_in"
        async with self.pool.acquire() as con:
            rows = await con.fetch(data)
            return [await self.get(i) for i in rows]

class Transaction(object):
    '''
        Сущность воплащающая в себе множество данных связанны с бизнес процессом (отличительная чертра - persistence,
        все ключевые моменты сохраняются в постоянное хранилище - в текущей реализации Postgres)
    '''

    pool = None  # пул соеднинений с бд

    def __init__(self, pool, **kwargs):
        """
        Этот метод должен вызываться из фабрики
        """
        self.pool = pool
        for i in kwargs.keys():
            if i in tables_columns:
                setattr(self, i, kwargs[i])

    async def save_state(self, state):
        """
        Меняем state транзакции с сохранением в бд
        :param state: состояние
        :return:
        """
        type_check(self.merchant_id, 'merchant_id')
        if state not in transaction_states:
            raise ValueError

        data = f"""UPDATE transaction_in SET state='{state}' WHERE  merchant_id = '{self.merchant_id}'"""
        async with self.pool.acquire() as con:
            await con.execute(data)
            self.state = state

    async def fixed(self):
        """
        Зачисление средств на внутренний счет с изменение состояния транзакции на fixed в одну транзакцию БД
        :param fixed_operation_func: Корутина которая добавляет операции в бд в текущую транзакцию
        :return:

        """
        wallet = Wallet.get_wallet_by_key(self.wallet_key)

        type_check(self.merchant_id, 'merchant_id')
        type_check(self.amount, 'amount')
        type_check(self.currency, 'currency')

        data = f"""UPDATE transaction_in SET state='fixed' WHERE  merchant_id = '{self.merchant_id}'"""
        async with self.pool.acquire() as con:
            async with con.transaction():
                await wallet.add(connection=con, amount=self.anount, currency=self.currency)
                await con.execute(data)

    async def save_task_id(self, task_id):
        """
        Сохрняет task_id транзакции в бд
        :param task_id:
        :return:
        """
        type_check(self.merchant_id, 'merchant_id')

        if task_id is not None:
            type_check(task_id, 'task_id')

        data = f'''UPDATE transaction_in SET task_id={task_id} WHERE  merchant_id = {self.merchant_id}'''
        async with self.pool.acquire() as con:
            await con.execute(data)
            self.task_id = task_id

    async def remove(self):
        """
        Удаляем транзакцию из БД
        :return:
        """
        type_check(self.merchant_id, 'merchant_id')
        data = f"""DELETE FROM transaction_in  WHERE  merchant_id = '{self.merchant_id}'"""
        async with self.pool.acquire() as con:
            await con.execute(data)