"""
В бизнес модели взаимодействия пять сущностей

TransactionIn - транзакция по вводу средств
SuperPay - взаимодействие с платежной системой
Wallet - внутренний счет пользователя
Workflow - Бизнесс процесс
Logger - журнал транзакций



"""

import flex
from flex.core import validate
from workflow import WorkflowPay
from transaction_pay import TransactionAbstractFactory, tables_columns


class Pay(object):
    """
    API для засисление средств  на депозит

    При инициализации системы вызвать init - он проедется по всем не завершенным транзакциями,
    если их можно завершить - завершит, если нельзя - удалит. При этом каждая транзакция обрабатывается в своей таске

    Для добавления новой траназкции необходимо вызвать transaction_create - он поместит транзакцию в БД, присвоет
    ей уникальный UUID. Вернет объект Task. Эта операция полностью выполняется в таске обрабатывающего запроса

    Для того чтобы запустить первичную обработку транзакции необходимо вызвать start_workflow, она создасть платеж в
    платежной системе и вернет URL на страницу в платежной системе для подтвержения платежа. Корутина выполняется в таске
    обрабатывающего запроса:

    >>> raise aiohttp.web.HTTPFound(Pay.start_workflow(Pay.transaction_create(pool,  wallet, amount, currency)))

    Запросы обрабатывющие callback от платежной системы обрабатываются в callback. Для  каждого из них созадется новая
    таска

    """
    @classmethod
    async def transaction_create(cls, pool,  wallet, amount, currency):

        """
        Создает новую транзакцию в БД
        :param pool: - пул соединений с бд
        :param wallet:  - внутренинй счет
        :param amount:  - сумма для зачисления
        :param currency: - валюта
        :return: Transaction
        """
        return await TransactionAbstractFactory(pool).create(wallet=wallet, amount=amount, currency=currency)

    @classmethod
    async def start_workflow(cls, transaction):
        """
         Начинает обработку транзакции
        :param transaction: - транзакция, созданная в transaction_create
        :return: url для перенаправления на страницу платежной системы или None (если траназкция уже обрабатывается)
        """
        return await WorkflowPay(transaction=transaction).workflow()

    @classmethod
    async def callback(cls, result, response, loop, swagger_file='schemas/callback.yaml'):
        """
        Обработка callback от платежной системы (завершение транзакции). Создает отдельные таски в loop
        :param result: success или failure
        :param response: JSON ответ от платежной системы
        :param loop: asyncio.loop - цикл в котором будет запущена таска
        :param swagger_file: описание response в сваггере
        :return:
        """
        schemas = flex.load(swagger_file)
        schema = schemas['definitions']['PayStatus']
        validate(schema, response)
        id_type = tables_columns['merchant_id']
        transaction = TransactionAbstractFactory.get(id_type(result['merchant_id']))
        if result is not None:
            r = True if result == 'success' else False
        else:
            r = None
        return loop.create_task(WorkflowPay(transaction=transaction).workflow(r))

    @classmethod
    async def init(cls, loop, pool):
        """
        Первичная инициализация обработки траназкции:
        Проходит по таблице транзакций. Если транзакция не найдена
        :param loop:
        :param pool:
        :return:
        """
        transactions = await TransactionAbstractFactory(pool).all()
        for i in transactions:
            loop.create_task(WorkflowPay(i).init())
