import aiohttp
import flex
from flex.core import validate


class SuperPayAbstractFactory(object):
    '''
        Абстрактная фабрика для генерации взаимодействия с платежной системой
    '''

    locale = None
    redirect_success_url = None
    redirect_failure_url = None
    client_session = None
    swagger_file = None
    schemas = None

    def __init__(self, client_session=aiohttp.ClientSession,
                 swagger_file='schemas/superpay.yaml',
                 redirect_success_url='https://example.com/superpay?result=success',
                 redirect_failure_url='https://example.com/superpay?result=failure',
                 locale='en'):
        self.client_session = client_session
        self.swagger_file = swagger_file
        self.schemas = flex.load(self.swagger_file)
        self.redirect_success_url = redirect_success_url
        self.redirect_failure_url = redirect_failure_url
        self.locale = locale

    def _swagger_validate(self, model_name, data):
        """
        Проверка данным на соответсвие сваггер модели
        :param model_name: Имя модели в сваггере
        :param data: данные для валидации
        :return: Ничего если проверка не удачна то кидается ValueError
        """
        schema = self.schemas['definitions'][model_name]
        validate(schema, data)

    def _endpoint_from_swagger(self, endpoint):
        """
        Фомрирования URL из swagger file
        :param endpoint: endpoint
        :return: url
        """
        # TODO нужнжо бы еще проверять наличие endpoint в сваггере
        s = self.schemas
        return f"{s['schemes']}://{s['host']}{s['basePath']}{endpoint}"

    async def create_pay(self, merchant_id, amount, currency, desc='some transaction description'):
        """
        Созадет новый платеж по зачислеию на депозит в платежной системе
        :param merchant_id: merchant_id транзакции
        :param amount:  число для зачисления
        :param currency: валюта
        :param desc: описание
        :return: token платежа в платежной системе
        """
        data = {
            'description': desc,
            'amount': amount,
            'currency': currency,
            'redirect_success_url': self.redirect_success_url,
            'redirect_failure_url': self.redirect_failure_url,
            'locale': self.locale,
            'merchant_id': merchant_id
        }

        self._swagger_validate('PaymentCreate', data)

        uri = self._endpoint_from_swagger('/deposit')

        async with self.client_session() as session:
            async with session.post(uri, json=data) as response:
                res = await response.json()
                self._swagger_validate('PayStatus', res)
                return res['status']

    def get_redirect_url(self, token):
        """
        Возвращает url для перенаправление в платежную систему
        :param token: token платежа в платежной системе
        :return: str
        """
        return f"{self._endpoint_from_swagger('/deposit/gui')}?merchant_id={token}"

    async def get_status(self, merchant_id):
        """
        Возварщает статус платежа в платежной системе
        :param merchant_id:
        :return: str статуса
        """
        data = {
            'merchant_id': merchant_id
        }

        self._swagger_validate('PayId', data)
        uri = self._endpoint_from_swagger('/deposit/gui')
        async with self.client_session() as session:
            async with session.post(uri, json=data) as response:
                res = await response.json()
                self._swagger_validate('PayStatus', res)
                return res['status']

    async def capture(self, merchant_id):
        """
        Подтвержение платежа после проведения в платежной системе
        :param merchant_id: id
        :return:
        """
        data = {
            'merchant_id': merchant_id
        }

        self._swagger_validate('PayId', data)
        uri = self._endpoint_from_swagger('/deposit/capture')
        async with self.client_session() as session:
            async with session.post(uri, json=data) as response:
                res = await response.json()
                self._swagger_validate('PayStatus', res)
                return res['status']
