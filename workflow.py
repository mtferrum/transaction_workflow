import asyncio
from Logger import Logger
from superpay import SuperPayAbstractFactory


class TransactionLockedException(Exception):
    """
     Транзакция которую пытаются заблокировать уже заблокирована существующей Task
    """

    def __init__(self, current_id, id_in_transaction, merchant_id):
        """

        :param current_id: id текущей таски
        :param id_in_transaction:  id таски в транзакции
        :param merchant_id: uuid Транзакции (для трассировки в бд
        """
        self.cur_id = current_id
        self.in_transaction = id_in_transaction
        self.merchant_id = merchant_id

    def __str__(self):
        return f"TransactionLocked: Transaction({self.merchant_id}); current Task({self.cur_id}); locked by Task({self.in_transaction})"


class TransactionError(Exception):
    """Базовый класс ошибок генерируемый WF"""

    def __init__(self, merchant_id):
        """
        :param merchant_id: UUID транзакци
        """
        self.waiting_state = waiting_state

    def __str__(self):
        return f"{self.__class__.__name__}: Transaction({self.merchant_id})"


class TransactionSyncError(TransactionError):
    """Ошибка синхронизации транзакций севриса и платежной системы"""


class TransactionPendingError(TransactionError):
    """Ошибка создания платежа в платежной системе"""


class TransactionLoggingError(TransactionError):
    """Ошибка в записи транзакции в журнал"""


class TransactionCaptureError(TransactionError):
    """Ошибка в подтвержение транзакции"""


class TransactionFixedError(TransactionError):
    """Ошибка в подтвержение транзакции"""


class TransactionNotFoundError(TransactionError):
    """Транзакция не найдена в платежной системе"""


class TransactionUndefinedStateError(TransactionError):
    """Ошибка алгоритма"""


class WorkflowPay(object):
    """"
    Бизнесс процесс пополения депозита.
    """

    transaction = None

    # подстроечнные параметры
    extra_check = True         # дополнительная проверка проведения платежа
    pending_try_times = 3      # количество попыток создать платеж в платежной системе
    capture_try_times = 3      # количество попыток подтвердить платеж
    extra_check_try_times = 3  # колличество попыток проверить платеж
    wait_time = 1              # время ожидания между попытками

    def __init__(self, transaction):
        """
        :param transaction: Объект транзакции
        """
        self.transaction = transaction

    async def lock(self):
        """Блокируем транзакцию для текущей таски. Если транзакция уже заблокирована таской, которая еще существует
        то возбуждается исключение:

        TransactionLocked
        """
        my_id = id(asyncio.Task.all_tasks())

        if self.transaction.task_id == my_id:
            return

        if self.transaction.task_id is None:
            self.transaction.save_task_id(my_id)
            return

        all_running_task = [id(task) for task in asyncio.Task.all_tasks()]
        if self.transaction.task_id not in all_running_task:
            self.transaction.save_task_id(my_id)
            return
        # Сюда мы попасть не должны!
        raise TransactionLockedException(my_id, self.transaction.task_id, self.transaction.merchant_id)

    async def unlock(self):
        """
        Освобождаем транзакцию от таски
        :return:
        """
        self.transaction.save_task_id(None)

    async def init(self):
        """
        Восстановление после перезапуска. Проверяет транзакцию
        если она not_found, created или pedning - то удаляет транзацкию
        иначе запускает workflow
        :return:
        """

        try:
            await self.lock()
        except TransactionLockedException:
            return

        pay = SuperPayAbstractFactory()
        transaction = self.transaction
        state_in_pay = await pay.get_status(merchant_id=transaction.merchant_id)

        if (state_in_pay in ('not_found', 'pedning')) or (transaction.state == 'created'):
            transaction.remove()
        else:
            self.workflow()

    async def workflow(self, callback_result=None):
        '''
                Основная корутина для воплощения workflow процесса зачисления на депозит
               :param callback_result: результат проведения операции на странице платежноый системы
                                       True - successed
                                       False - failure
                                       None - первичны запуск
               :return: url для перенпаравления на платежную страницу, если платеж был успешно создан в платежной системе
                        None - если транзакция заблокирована
                        True - если транзакция успешно проведена

               Логика бизнес процесса зачисления на депозит

               1. Блокируем транзакцию себе (дабы никто более не обрабатывал ее)
               2  Проверяем является ли текущий вызов callback
               2.1 Если не является то промежуточных состояний дополнительно можем обновить значение из платежной системы
               3. Формируем платеж
               4. возвращаем url на стороне платежной системы для перенаправления пользователя

                   <вот здесь в принципе разрыв с окончанием корутины и в принципе остальные шаги можно делать в отдельной корутине,
                  но мне кажется что сделать это здесь логичней, это позволит в случае если систем умрет восстановить работу
                   запуском одной и той же корутины для всех незавершенных транзакций вне зависимости от стадии их выполения >

               5. второй заход в корутину
               5.1 платеж отклонен
               5.2 платежная система ждет подтвержение платежа
               6. проверяем
               7. Зачисляем на внутренний счет
               8. логгируем
               9. удаляем транзакцию из бд
               ----- Особые случаи ------
               10. Транзакця не найдена в платежной системе
               11. Неизвестное состояние транзакции
               исключения:
               TransactionPendingError - не удалось создать платеж в платежной системе
               TransactionLoggingError - ошибка в записи транзакции в журнал
               TransactionCaptureError - ошибка в подтвержение транзакции
               TransactionSyncError - ошибка в рассогласованности данных с платежной системой
               TransactionFixedError - ошибка в начисление средств на внутренний счет
               TransactionNotFoundError - транзакция не найдена в платежной системе
               TransactionUndefinedStateError - ошибка алгоритма
        '''

        # 1
        try:
            await self.lock()
        except TransactionLockedException:
            #Транзакция уже обрабатывается
            return

        pay = SuperPayAbstractFactory()
        transaction = self.transaction

        # 2
        if callback_result is not None:
            if self.extra_check:
                transaction.state = await pay.get_status(merchant_id=transaction.merchant_id)
            else:
                if callback_result:
                    transaction.state = 'waiting_for_capture'
                else:
                    transaction.state = 'canceled'
        # 2.1
        else:
            if self.extra_check:
                if transaction.state not in ('created', 'fixed', 'logging'):
                    transaction.state = await pay.get_status(merchant_id=transaction.merchant_id)
        # 3
        if transaction.state == 'created':
            for i in range(self.pending_try_times):
                transaction.state = await pay.create_pay(merchant_id=transaction.merchant_id,
                                                         amount=transaction.amount,
                                                         currency=transaction.currency)
                if transaction.state == 'pending':
                    break
                asyncio.sleep(self.wait_time)

            if transaction.state != 'pending':
                # не удалось создать платеж в платежной системе
                raise TransactionPendingError(transaction.merchant_id)
        # 4
        if transaction.state == 'pending':
                await self.unlock()
                return pay.get_redirect_url(transaction.merchant_id)

        # 5.1
        if transaction.state == 'canceled':
            await Logger.log(transaction=transaction)
            if transaction.state != 'logged':
                raise TransactionLoggingError(merchant_id=transaction.merchant_id)

        # 5.2
        if transaction.state == 'waiting_for_capture':

            for i in range(self.capture_try_times):
                transaction.state = await pay.capture(merchant_id=transaction.merchant_id)
                if transaction.state != 'waiting_for_capture':
                    break
                asyncio.sleep(self.wait_time)

            if transaction.state == 'waiting_for_capture':
                raise TransactionCaptureError(transaction.merchant_id)

            if self.extra_check:
                for i in range(self.extra_check_try_times):
                    transaction.state = await pay.get_status(merchant_id=transaction.merchant_id)
                    if transaction.state == 'succeeded':
                        break
                asyncio.sleep(self.wait_time)

        # 6
            if transaction.state != 'succeeded':
                raise TransactionSyncError(merchant_id=transaction.merchant_id)
            else:
                transaction.save_state(transaction.state)
        # 7
        if transaction.state == 'succeeded':
            await transaction.fixed()
            if transaction.state != 'fixed':
                raise TransactionFixedError(merchant_id=transaction.merchant_id)

        # 8
        if transaction.state == 'fixed':
            await Logger.log(transaction=transaction)
            if transaction.state != 'logged':
                raise TransactionLoggingError(merchant_id=transaction.merchant_id)
        # 9
        if transaction.state == 'logged':
            await self.transaction.remove()
            return True
        # 10
        if transaction.state == 'not_found':
            raise TransactionNotFoundError(me)
        # 11
        raise TransactionUndefinedStateError(merchant_id=transaction.merchant_id)
